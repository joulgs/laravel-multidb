<?php

namespace App\Console\Commands\Tenant;

use App\Models\SystemCliente;
use App\Tenant\ManagerTenant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class TenantMigrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:migrations {id?} {--refresh} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executa as Migrations dos Tenants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ManagerTenant $tenant) 
    {
        parent::__construct();

        $this->tenant = $tenant;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->argument('id'))
        {
            $cliente = SystemCliente::find($this->argument('id'));

            if($cliente){ $this->execComando($cliente); } else { $this->info(" Nenhuma empresa encontrada com esse ID "); }

            return;
        }

        $clientes = SystemCliente::all();

        foreach ($clientes as $cliente)
        {
            
            $this->execComando($cliente);

        }
        
    }

    public function execComando(SystemCliente $cliente)
    {
        $comando = $this->option('refresh') ? 'migrate:refresh' : 'migrate';

        $this->tenant->setConnection($cliente);

        $this->info(" Conectado no Cliente {$cliente->nome} ");

        Artisan::call($comando,[
            '--force' => 'true',
            '--path' => 'database/migrations/tenant',
        ]);

        $this->info(" Terminado em {$cliente->nome} ");
        $this->info(" ---------------------------------------- ");
    }
    
}
