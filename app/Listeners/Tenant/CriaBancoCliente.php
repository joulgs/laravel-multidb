<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\BancoCriado;
use App\Events\Tenant\ClienteCadastrado;
use App\Tenant\Database\DatabaseManager;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CriaBancoCliente
{
    private $database;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DatabaseManager $database)
    {
        //
        $this->database = $database;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ClienteCadastrado $evento)
    {
        $cliente = $evento->cliente();

        if(!$this->database->createDatabase($cliente))
        {
            throw new Exception('Erro ao Criar Base de Dados');
        }

        event( new BancoCriado($cliente) );
    }
}
