<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\BancoCriado;
use App\Events\Tenant\TabelasCriadas;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;

class RunMigrationsTenant
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BancoCriado  $event
     * @return void
     */
    public function handle(BancoCriado $event)
    {
        //
        $cliente = $event->cliente();

        Artisan::call('tenant:migrations', [
                'id' => $cliente->id,
        ]);        
    }   
}
