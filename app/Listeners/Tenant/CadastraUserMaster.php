<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\TabelasCriadas;
use App\Models\SystemCliente;
use App\Models\User;
use App\Tenant\ManagerTenant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CadastraUserMaster
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ManagerTenant $tenant)
    {
        //
        $this->tenant = $tenant;
    }

    /**
     * Handle the event.
     *
     * @param  TabelasCriadas  $event
     * @return void
     */
    public function handle(TabelasCriadas $event)
    {
        //

        $cliente = $event->cliente();

        User::create([
            'name' => $cliente->nome,
            'email' => $cliente->email,
            'password' => bcrypt($cliente->senha),
        ]);

    }

}
