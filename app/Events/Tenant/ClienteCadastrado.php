<?php

namespace App\Events\Tenant;

use App\Models\SystemCliente;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ClienteCadastrado
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $cliente;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SystemCliente $cliente)
    {
        //dd('aki');
        $this->cliente = $cliente;
    }

    //função que retorna o cliente
    public function cliente()
    {
        return $this->cliente;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
