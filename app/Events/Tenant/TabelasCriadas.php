<?php

namespace App\Events\Tenant;

use App\Http\Requests\StoreSystemClienteRequest;
use App\Models\SystemCliente;
use GuzzleHttp\Psr7\Request;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TabelasCriadas
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $cliente;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StoreSystemClienteRequest $cliente)
    {
        $this->cliente = $cliente;
    }

    public function cliente()
    {
        return $this->cliente;
    }

    public function clienteConnection($cnp)
    {
        return SystemCliente::where('cnp', $cnp)->first();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
