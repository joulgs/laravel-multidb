<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Tenant\ClienteCadastrado' => [
            'App\Listeners\Tenant\CriaBancoCliente',
        ],
        'App\Events\Tenant\BancoCriado' => [
            'App\Listeners\Tenant\RunMigrationsTenant',
        ],
        'App\Events\Tenant\TabelasCriadas' => [
            'App\Listeners\Tenant\CadastraUserMaster',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
