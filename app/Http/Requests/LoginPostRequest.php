<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cnp' => 'required',
            'email' => 'required',
            'senha' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cnp.required' => 'CNPJ é necessario',
            'email.required' => 'Email é necessario',
            'senha.required' => 'Senha é Necessaria'
        ];
    }
}
