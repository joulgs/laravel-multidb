<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSystemClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//return false; true para poder permitir
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razao' => 'required|min:3|max:80',
            'cnpj' => 'required|min:11|max:14'
        ];
    }

    public function messages()
    {
        return [
            'razao.required' => 'Razão Social é Obrigatoria'
        ];
        
    }
}
