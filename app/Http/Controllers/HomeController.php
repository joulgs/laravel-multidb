<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); // SE ATIVAR REDIRECIONA PARA O LOGIN MESMO LOGADO
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($cnpempresa)
    {
        //dd(Auth::check()); //retirar metodo AUTH futuramente apos resolver problemas dos usuarios
        //estou tentando descobrir por que o login está valido para todos os tenant
        //o login e o AUTH ja está funcionando
        //dd('123');
        //dd(session('sessionCNP'));

        return view('dashboard.home');
    }
}
