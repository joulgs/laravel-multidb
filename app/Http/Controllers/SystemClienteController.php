<?php

namespace App\Http\Controllers;

use App\Events\Tenant\ClienteCadastrado;
use App\Events\Tenant\TabelasCriadas;
use App\Http\Requests\StoreSystemClienteRequest;
use App\Models\SystemCliente;
use Dotenv\Dotenv;
use Illuminate\Http\Request;


class SystemClienteController extends Controller
{

    protected $request;
    protected $cliente;

    public function __construct(Request $request, SystemCliente $cliente)
    {
        $this->request = $request;
        $this->cliente = $cliente;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('site.cadastrar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSystemClienteRequest $request)
    {
        $cliente = $this->cliente->create([ 
            'nome' => $request->razao,
            'cnp' => $request->cnpj,
            'db_database' => 'prefix_'.$request->cnpj,
            'db_host' => 'mysql',
            'db_user' => 'root',
            'db_password' => 'root',
        ]); 

        event( new ClienteCadastrado($cliente) ); //dispara o evento que irá criar o banco de dados do cliente
        
        event( new TabelasCriadas($request) ); //dispara o evento que criará o primeiro usuario no banco do cliente

        return redirect()->route('login' , $request->cnpj); //redireciona o cliente para a tela de login
        
    }

    public function login($cnpempresa = ''){
        
        
        return view('site.login' , [
            'cnp' => $cnpempresa
            ]);
    }

    public function getCliente()
    {
        if(!$cliente = $this->cliente->where('cnp', $this->request->cnp)->first())
            return dd('não tem conta cadastrada');
        

        return redirect()->route('login' , $this->request->cnp);
        
        
    }
}
