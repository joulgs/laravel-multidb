<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use League\CommonMark\Inline\Element\Code;

class LoginController extends Controller
{
    protected $cnp;
    protected $email;
    protected $senha;
    
    //methodo que irá receber os dados de login, identificar se existe o banco de dados, e logar
    public function autenticar(Request $request)
    {
        
        $this->cnp = $request->segment(1);
        $this->email = $request->email;
        $this->senha = $request->senha;


        $credentials = ['email' => $this->email, 'password' => $this->senha]; 
            
        if ($user = Auth::attempt($credentials))
        {
            session(['sessionCNP' => $this->cnp]);
            return redirect()->route('sistema.home',$this->cnp);

        } else {
            dd('Acesso incorreto');
        }

    }
    
}
