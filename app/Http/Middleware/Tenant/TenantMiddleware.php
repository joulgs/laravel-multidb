<?php

namespace App\Http\Middleware\Tenant;

use App\Models\SystemCliente;
use App\Tenant\ManagerTenant;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $cnp = $request->segment(1);

        $cliente = $this->getCliente($cnp);
        
        if (!$cliente)
        {
            dd('Tenant Middleware não encontrou cliente');
        } else {

            $cnpLogado = session('sessionCNP');
            if( isset( $cnpLogado ) && $cnpLogado != $cnp )
            {
                Auth::logout(); 
                session()->flush();
                
                return $next($request);
            }
            


            app(ManagerTenant::class)->setConnection($cliente); //Seta Conexão com o banco de dados
        }
        return $next($request);
    }

    //
    public function getCliente($cnp)
    {
        return SystemCliente::where('cnp', $cnp)->first();
    }
}

