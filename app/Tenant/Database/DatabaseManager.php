<?php

namespace App\Tenant\Database;

use App\Models\SystemCliente;
use Illuminate\Support\Facades\DB;

class DatabaseManager
{
    public function createDatabase(SystemCliente $cliente)
    {
        return DB::statement("CREATE DATABASE {$cliente->db_database} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"); 
    }
}