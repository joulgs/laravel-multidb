<?php 

namespace App\Tenant;

use App\Models\SystemCliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ManagerTenant 
{

    /**
     * Esse metodo irá alterar a conexão de banco de dados dinamicamente
     * */    
    public function setConnection(SystemCliente $cliente) 
    {
        DB::purge('tenant'); //remove possiveis configurações de banco

        config()->set('database.connections.tenant.host', $cliente->db_host);
        config()->set('database.connections.tenant.database', $cliente->db_database);
        config()->set('database.connections.tenant.username', $cliente->db_user);
        config()->set('database.connections.tenant.password', $cliente->db_password);

        DB::reconnect('tenant');

        Schema::connection('tenant')->getConnection()->reconnect();
    }

}