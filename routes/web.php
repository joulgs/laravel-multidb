<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SystemClienteController;
use App\Http\Controllers\LoginController;


//Site Inicial
Route::get('/', function () {       
    return view('site.home');
})->name('site.home');

//Rotas de Cadastro de novo usuario
Route::get('/NovoCadastro', [SystemClienteController::class, 'create'])->name('novoCadastro');
Route::post('/NovoCadastro', [SystemClienteController::class, 'store'])->name('novoCadastro.store');

//Rota de Login Sem CNP
Route::get('/login', function(){ return view('site/login-semcnp'); })->name('login.semcnp');
Route::post('/login', [SystemClienteController::class, 'getCliente'])->name('login.busca');


Route::group(['middleware' => 'tenant.middleware'], function(){

    //Rotas de Login com o CNPJ informado da URI
    Route::get('/{cnp?}/login', [SystemClienteController::class, 'login'])->name('login');
    Route::post('/{cnp}/login', [LoginController::class, 'autenticar'])->name('autenticar');

    //Rotas do sistema 
    Route::get('/{cnp}/sistema/', [HomeController::class, 'index'])->name('sistema.home');

});

//Auth::routes();
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
