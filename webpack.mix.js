const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/main.scss', 'public/assets/css')
    .scripts([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/startbootstrap-sb-admin-2/vendor/jquery-easing/jquery.easing.min.js',//necessario para rodar o scrollToTop do SB Admin
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/@fortawesome/fontawesome-free/js/all.js',
        'node_modules/vue/dist/vue.js',
        'node_modules/startbootstrap-sb-admin-2/js/sb-admin-2.min.js'
        ], 'public/assets/js/vendor.js');
