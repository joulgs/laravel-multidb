
## Colocando para funcionar

- 1 - clone o projeto para uma nova pasta
- 2 - execute o composer update --ignore-platform-reqs
- 3 - execute o npm install
- 4 - execute o npm run dev 
- 5 - entre no arquivo .env e altere o DB_DATABASE para o nome do banco de dados matriz
- 6 - entre no arquivo .env e altere o DB_CONNECTION para 'tenant'
- 7 - execute o php artisan migrate
- 8 - crie um usuario no banco master

