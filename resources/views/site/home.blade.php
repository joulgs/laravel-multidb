@extends('site/layout/layout')

@section('title', 'Home')



@section('Principal') {{--Seção do Conteudo do Layout utilizado--}}

    @include('site/components/menu') {{--Menu Superior do Site--}}

    <main role="main" class="container">
    

        <div class="starter-template">
            <h1>Testes</h1>
            <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
        </div>
    </main>

@endsection

@push('styles')
    <style>
        body{
            padding-top: 5rem;
        }

        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
    </style>
    
@endpush