@extends('site/layout/layout')

@section('title','Cadastrar')
    
@section('Principal')

    @include('site.includes.back')

    
    <form action="{{ route('novoCadastro.store') }}" method="post" class="form-signin">
        
        @component('site.components.logoFormHead')
            <h1 class="h3 mb-3 font-weight-normal">Criar uma nova conta gratuita</h1>
            <p class="mb-5 mt-3">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi provident reprehenderit excepturi ipsam! Quis maxime</p>
        @endcomponent

        @include('site.includes.alerts')

        @csrf
        <input class="form-control" type="text" name="cnpj" placeholder="CNPJ/CPF" value="11122233344">
        <input class="form-control" type="text" name="razao" placeholder="Razao Social" value="Cliente 001">
        <input class="form-control" type="text" name="fantasia" placeholder="Fantasia" value="Loja 001">
        
        <p class="mt-3">Agora Informe os Seus Dados</p>
        <input class="form-control" type="text" name="nome" placeholder="Nome" value="NomeBlabla">
        <input class="form-control" type="text" name="sobrenome" placeholder="Sobrenome" value="SobreNome">
        <input class="form-control" type="text" name="email" placeholder="e-mail" value="teste@teste.com">
        <input class="form-control" type="password" name="senha" placeholder="Senha de acesso" value="123456">

        <div class="checkbox mb-2 mt-3">
            <label>
            <input type="checkbox" value="remember-me"> Declaro que Li e aceito os <a href="#">Termos de uso</a>
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    </form>
    
@endsection

@push('styles')

    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            -ms-flex-align: center;
            -ms-flex-pack: center;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 700px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .checkbox {
            font-weight: 400;   
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .center {
            text-align: center
        }

    </style>
    
@endpush
    
