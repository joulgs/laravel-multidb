@extends('site/layout/layout')

@section('title', 'Login')

@section('Principal')

@include('site.includes.back')

<form class="form-signin" action="{{ route('login',$cnp) }}" method="post">
    
    @component('site.components.logoFormHead')
        <h1 class="h3 mb-3 font-weight-normal">Acessar o sistema</h1>
    @endcomponent
    
    @csrf
    
    <label class="sr-only">Email</label>
    <input type="email" name="email" class="form-control" placeholder="E-mail" required>
    
    <label class="sr-only">Senha</label>
    <input type="password" name="senha" class="form-control" placeholder="Senha" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Lembrar
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    
</form>
    
@endsection

@push('styles')

    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            -ms-flex-align: center;
            -ms-flex-pack: center;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .checkbox {
            font-weight: 400;   
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .center {
            text-align: center
        }

    </style>
    
@endpush