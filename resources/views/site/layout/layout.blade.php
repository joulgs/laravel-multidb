<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="">
    <meta name="description" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>@yield('title')</title>

    <link rel="stylesheet" href=" {{ asset('assets/css/main.css') }} "> {{--Folha de estilo geral--}}

    @stack('styles')
  </head>
  <body>
      <div id="app">

        @yield('Principal')

        </div>
    </main>
    <script src=" {{ asset('assets/js/app.js') }} "></script>
    <script src=" {{ asset('assets/js/vendor.js') }} "></script>

    @stack('scripts')
    
    <script>

    </script>
</body>
</html>